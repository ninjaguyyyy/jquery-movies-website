## Về Project
- Thuộc môn học Phát triển ứng dụng web
- Chủ đề: Xem thông tin phim.
- Loại: Front end JQuery và Vanilla Js
- Backend: dịch vụ cung cấp api https://api.themoviedb.org

## Ảnh demo

<img alt="demo" src="https://res.cloudinary.com/dwuma83gt/image/upload/v1601541645/x_jnofz5.jpg" />
<img alt="demo" src="https://res.cloudinary.com/dwuma83gt/image/upload/v1601541630/y_pnwkpv.jpg" />
<img alt="demo" src="https://res.cloudinary.com/dwuma83gt/image/upload/v1601541630/z_ylu1dg.jpg" />

## Các chức năng
- Hiển thị danh sách phim.
- Hiển thị chi tiết phim.
- Hiển thị thông tin diễn viên.
- Tìm kiếm (theo tên phim, tên diễn viên).

## Các kiến thức học được