const key = '6f5d73a3c5cb4b7c43bd01d2f9693a39';

const imgURL = 'https://image.tmdb.org/t/p/original';

$(window).on("load", async function () {

    let cast = await getCast();
    const idPerson = cast.person.id;
    let people = await getPeople(idPerson);
    let images = await getImages(idPerson); 
    let filmography = await getFilmography(idPerson); 

    createOutputPeopleDetail(people, images, filmography);

});

function createMoreNfo(people) {
    return `
        <div class="col-sm-2">
            <div class="moreNfo">
                
                <div class="moreNfo__item">
                    <div class="moreNfo__title">Fullname:</div>
                    <a href="#" class="link moreNfo__content">${people.name}</a>
                </div>
                <div class="moreNfo__item">
                    <div class="moreNfo__title">Date Of Birth:</div>
                    <span href="#" class="link moreNfo__content">${people.birthday}</span>
                </div>
                <div class="moreNfo__item">
                    <div class="moreNfo__title">Country:</div>
                    <span href="#" class="link moreNfo__content">${people.place_of_birth}</span>
                </div>
                <div class="moreNfo__item">
                    <div class="moreNfo__title">Gender:</div>
                    <span href="#" class="link moreNfo__content">${people.gender === 2 ? 'Male': 'Female'}</span>
                </div>
            </div>
        </div>
    `
};

function createfilmography(casts, type) {

    let castArray;
    if(type !== 'full') {
        castArray = casts.length > 10 ? casts.slice(0, 10) : [...casts];
    }
    else {
        castArray = [...casts];
    }

    return `

        <div class="castList">
            ${
                (type === 'full')?
                `<div class="heading mt-3 mb-3">
                    <div class="heading__title">Filmography</div>
                </div>`
                :
                null
            }
            
            ${castArray.map(cast => `
                <div class="cast">
                    <div class="cast__avatar mr-3">
                        <img src="${imgURL + cast.poster_path}" class="img-fluid" alt="avatar">
                    </div>
                    <a href="#" class="link cast__name">${cast.title}</a>
                    <div class="cast__character">Character: <span>${cast.character}</span></div>
                </div>
            `).join('')}
        </div>
    `
};

function createImage(imgs) {
    return `

        <div class="library">
        ${imgs.map(img => `
                <img src="${imgURL + img.file_path}" alt="item1" class="library__item">
            `).join('')}
            
        </div>
    `
};

function createOutputPeopleDetail(people, images, filmography) {

    $('.info-top__title').html(people.name);
    $('.info-top__role').html(people.known_for_department);
    $('.movieNfo .card img').attr("src",imgURL + people.profile_path);

    let peopleDetailOutput = `
        <div class="col-sm-4"></div>
        <div class="col-sm-6 pl-5">
                <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-overview">
                        ${people.biography}
                        <div class="heading mt-3">
                            <div class="heading__title">video & photos</div>
                            <a class="link heading__link" href="javascript:void(0)">all video & photos ></a>
                        </div>
                        ${createImage(images.profiles)}
                    
                        <div class="heading mt-3">
                            <div class="heading__title">filmography</div>
                            <a class="link heading__link filmography-link" href="javascript:void(0)">full cast ></a>
                        </div>
                        ${createfilmography(filmography.cast)}
                    </div>
                        
                    <div class="tab-pane fade" id="nav-filmography">
                        <div class="heading-reviews mb-4">
                            <p class="heading-reviews__title">Filmography of</p>
                            <h4 class="heading-reviews__movieName ml-4">This Person</h4>
                        </div>
                        ${createfilmography(filmography.cast, 'full')}
                    </div>
                    
                </div>

                
        </div>
        
        
    `+ createMoreNfo(people);


    $('#main-movie .row').empty();
    $('#main-movie .row').append(peopleDetailOutput);

    $('#nav-filmography-tab').click(function(){
        $('#nav-tab .nav-link').removeClass('active');
        $(this).addClass('active');

        $('#nav-tabContent .tab-pane').removeClass('show active');
        $('#nav-filmography').addClass('show active');
    });

    $('#nav-overview .filmography-link').click(function () {
        $('#nav-tab .nav-link').removeClass('active');
        $('#nav-filmography-tab').addClass('active');

        $('#nav-tabContent .tab-pane').removeClass('show active');
        $('#nav-filmography').addClass('show active');
    });

    $('#nav-overview-tab').click(function(){
        $('#nav-tab .nav-link').removeClass('active');
        $(this).addClass('active');

        $('#nav-tabContent .tab-pane').removeClass('show active');
        $('#nav-overview').addClass('show active');
    });
}

async function getCast() {
    const idCast = sessionStorage.getItem('idCastDetail');

    let response = await fetch(`https://api.themoviedb.org/3/credit/${idCast}?api_key=${key}`);
    let cast = await response.json();

    return cast;
}

async function getPeople(id) {
    let response = await fetch(`https://api.themoviedb.org/3/person/${id}?api_key=${key}`);
    let people = await response.json();

    return people;
};

async function getFilmography(id) {
    let response = await fetch(`https://api.themoviedb.org/3/person/${id}/movie_credits?api_key=${key}`);
    let filmography  = await response.json();

    return filmography;
}

async function getImages(id) {
    let response = await fetch(`https://api.themoviedb.org/3/person/${id}/images?api_key=${key}`);
    let images = await response.json();

    return images;
};