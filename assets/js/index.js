
// key: 6f5d73a3c5cb4b7c43bd01d2f9693a39
// image: https://image.tmdb.org/t/p/original/kqjL17yufvn9OVLyXYpvtyrFfak.jpg

// Biến Global
const key = '6f5d73a3c5cb4b7c43bd01d2f9693a39';
const imgURL = 'https://image.tmdb.org/t/p/original';
const limitPage = 10;
const limitPeople = 10;

$(window).on("load", async function () {
    renderLoading();

    let moviesResponse = await getPopularMovies(1);
    createOutput(moviesResponse);

    // Các xí lí phân trang
    createPagination(moviesResponse.total_pages);

    handlePagination('popular', moviesResponse);

    // chức năng tìm kiếm movies
    $('#searchField').on('submit', (event) => {
        renderLoading();
        let searchText = $('#searchText').val();
        getSearchedMovies(searchText, 1).then(moviesResponse => {
            
            createOutput(moviesResponse, 'search');
            createPagination(moviesResponse.total_pages);
            handlePagination('search', moviesResponse, searchText);

        });

        event.preventDefault();
    });

    // chuc nang search movies theo dien vien
    $('#searchFieldCast').on('submit', (event) => {
        renderLoading();
        let searchText = $('#searchTextCast').val();
        
        // lay 1 nguoi thoi
        getSearchedPeople(searchText, 1).then(async peopleResponse => {
            const people = peopleResponse.results[0];
            let mediaAll = await getFilmography(people.id);
            let films = mediaAll.cast;

            const moviesOutput = createMovieList(films);
            $(".main__wrapper .row").empty();
            $('.main__wrapper .row').append(moviesOutput);

            // thanh topbar
            $('.topBar__highlight').text(films.length + ' movies')
            
            createPagination(films.length/20 + 1);

        });

        event.preventDefault();
    });

});


function createLoading() {
    return `
        <div class="loading loading--show">
            <div class="spinner-grow text-primary" role="status">
                <span class="sr-only">Loading...</span>
            </div>
            <div class="spinner-grow text-secondary" role="status">
                <span class="sr-only">Loading...</span>
            </div>
            <div class="spinner-grow text-success" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>
    `
}

function renderLoading() {
    $(".main__wrapper .row").empty();
    $('.main__wrapper .row').append(createLoading());
}

function createMovieCard(movie) {
    return `
        <div class="col-sm-2">
            <div class="card">
                <img src="${imgURL+movie.poster_path}" class="card-img-top" alt="poster">
                <div class="card-body">
                    <p class="card-text"><a href="movie.html" onclick="selectedDetail('${movie.id}')" >${movie.title}</a></p>
                    <p class="score"><i class="fas fa-star"></i> <span>${movie.vote_average}</span> /10</p>
                </div>
            </div>
        </div>
    `
};

function createMovieList(movies) {
    let output = '';
    $.each(movies, (index, movie) => 
        output += createMovieCard(movie)
    );
    return output;
}

function createOutput(moviesResponse) {

    const { results: movies, total_results} = moviesResponse;

    // danh sách card
    const moviesOutput = createMovieList(movies);
    $(".main__wrapper .row").empty();
    $('.main__wrapper .row').append(moviesOutput);

    // thanh topbar
    $('.topBar__highlight').text(total_results + ' movies')

    
}

function handlePagination(type, moviesResponse, searchText) {
    if (type === 'popular') {
        $('.pagination li.current-page').on("click", (e) => {
            getPopularMovies(e.target.textContent).then(moviesResponse => createOutput(moviesResponse));
            
            if (e.currentTarget.classList.contains('active')) {
                return false;
            }
            else {
                $('.pagination li').removeClass('active');
                e.currentTarget.classList.add('active');
                const pageCurrent =  e.target.textContent;
                $('#quantity').html(`Page ${pageCurrent} of <span>${moviesResponse.total_pages > 10 ? 10 : moviesResponse.total_pages}</span>`)
            }
        });
    
        $('.pagination li.next').on("click", (e) => {
    
            const pageCurrent = $('.pagination li.active').children().text();
            if(pageCurrent === '10') {
                return false;
            }
            else {
                const elementCurrent = $('.pagination li.active');
                getPopularMovies(elementCurrent.next().children().text()).then(moviesResponse => createOutput(moviesResponse))
                
                $('.pagination li').removeClass('active');
                elementCurrent.next().addClass('active');
                const pageCurrent = elementCurrent.next().children().text();
    
                $('#quantity').html(`Page ${pageCurrent} of <span>${moviesResponse.total_pages > 10 ? 10 : moviesResponse.total_pages}</span>`)
            }
        });
    
        $('.pagination li.previous').on("click", (e) => {
    
            const pageCurrent = $('.pagination li.active').children().text();
            if(pageCurrent === '1') {
                return false;
            }
            else {
                const elementCurrent = $('.pagination li.active');
                getPopularMovies(elementCurrent.prev().children().text()).then(moviesResponse => createOutput(moviesResponse))
                
                $('.pagination li').removeClass('active');
                elementCurrent.prev().addClass('active');
    
                const pageCurrent = elementCurrent.prev().children().text();
                
                $('#quantity').html(`Page ${pageCurrent} of <span>${moviesResponse.total_pages > 10 ? 10 : moviesResponse.total_pages}</span>`)
            }
        });
    }
    // search movie
    else {
        $('.pagination li.current-page').on("click", (e) => {
            getSearchedMovies(searchText, e.target.textContent).then(moviesResponse => createOutput(moviesResponse));
            
            if (e.currentTarget.classList.contains('active')) {
                return false;
            }
            else {
                $('.pagination li').removeClass('active');
                e.currentTarget.classList.add('active');
                const pageCurrent =  e.target.textContent;
                $('#quantity').html(`Page ${pageCurrent} of <span>${moviesResponse.total_pages > 10 ? 10 : moviesResponse.total_pages}</span>`)
            }
        });
    
        $('.pagination li.next').on("click", (e) => {
    
            const pageCurrent = $('.pagination li.active').children().text();
            if(pageCurrent === '10') {
                return false;
            }
            else {
                const elementCurrent = $('.pagination li.active');
                getSearchedMovies(searchText, elementCurrent.next().children().text()).then(moviesResponse => createOutput(moviesResponse))
                
                $('.pagination li').removeClass('active');
                elementCurrent.next().addClass('active');
                const pageCurrent = elementCurrent.next().children().text();
    
                $('#quantity').html(`Page ${pageCurrent} of <span>${moviesResponse.total_pages > 10 ? 10 : moviesResponse.total_pages}</span>`)
            }
        });
    
        $('.pagination li.previous').on("click", (e) => {
    
            const pageCurrent = $('.pagination li.active').children().text();
            if(pageCurrent === '1') {
                return false;
            }
            else {
                const elementCurrent = $('.pagination li.active');
                getSearchedMovies(searchText, elementCurrent.prev().children().text()).then(moviesResponse => createOutput(moviesResponse))
                
                $('.pagination li').removeClass('active');
                elementCurrent.prev().addClass('active');
    
                const pageCurrent = elementCurrent.prev().children().text();
                
                $('#quantity').html(`Page ${pageCurrent} of <span>${moviesResponse.total_pages > 10 ? 10 : moviesResponse.total_pages}</span>`)
            }
        });
    }
    
}

async function getPopularMovies(pageNumber) {
    let response = await fetch(`https://api.themoviedb.org/3/movie/popular?api_key=${key}&language=en-US&page=${pageNumber}`);
    let moviesWrapper = await response.json();

    return moviesWrapper;
}

async function getSearchedMovies(queryString, pageNumber) {
    let response = await fetch(`https://api.themoviedb.org/3/search/movie?api_key=${key}&language=en-US&page=${pageNumber}&include_adult=false&query=${queryString}`);
    let moviesWrapper = await response.json();

    return moviesWrapper;
}

// dùng session storage để truyền key-value (data) giữa các page
function selectedDetail(idString) {
    sessionStorage.setItem('idMovieDetail', idString);
}

function createPagination(total) {
    let realTotal = total > limitPage ? limitPage : total;

    let paginationOutput = `
        <p class="topBar__info" id="quantity">Page 1 of <span>${realTotal}</span></p>
        <nav id = "">
            <ul class="pagination">
                <li class="page-item previous">
                    <a class="page-link" href="javascript:void(0)" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </li>
                <li class="page-item current-page active">
                    <a class="page-link" href="javascript:void(0)">1</a>
                </li>

                <li class="page-item next">
                    <a class="page-link" href="javascript:void(0)" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                </li>
            </ul>
        </nav>
        
    `;

    $('#nav-pagination').empty();
    $('#nav-pagination').append(paginationOutput);

    for(let i = 2; i <= realTotal; i++) {

        $('.pagination li.next').before(`
            <li class="page-item current-page">
                <a class="page-link" href="javascript:void(0)">${i}</a>
            </li>
        `)
    };

}

async function getSearchedPeople(queryString, pageNumber) {
    let response = await fetch(`https://api.themoviedb.org/3/search/person?api_key=${key}&language=en-US&page=${pageNumber}&query=${queryString}`);
    let peopleWrapper = await response.json();

    return peopleWrapper;
}

async function getPeople(id) {
    let response = await fetch(`https://api.themoviedb.org/3/person/${id}?api_key=${key}`);
    let people = await response.json();

    return people;
};

async function getFilmography(id) {
    let response = await fetch(`https://api.themoviedb.org/3/person/${id}/movie_credits?api_key=${key}`);
    let filmography  = await response.json();

    return filmography;
}