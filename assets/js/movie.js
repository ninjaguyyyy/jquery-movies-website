
const key = '6f5d73a3c5cb4b7c43bd01d2f9693a39';
const imgURL = 'https://image.tmdb.org/t/p/original';

$(window).on("load", async function () {

    let movieResponse = await getMovie();
    let casts = await getCasts();
    let images = await getImages();
    let reviewsResponse = await getReviews();

    createOutputMovieDetail(movieResponse, casts, images, reviewsResponse);
});

function createMoreNfo(movieResponse, directors) {
    const { genres, release_date, runtime } = movieResponse;

    return `
        <div class="col-sm-2">
            <div class="moreNfo">
                
                <div class="moreNfo__item">
                    <div class="moreNfo__title">Directors</div>
                    ${directors.map(director => `<span href="#" class="link moreNfo__content">${director.name} , </span>`).join('')}
                </div>
                <div class="moreNfo__item">
                    <div class="moreNfo__title">Genres</div>
                    ${genres.map(genre => `<span href="#" class="link moreNfo__content">${genre.name}, </span>`).join('')}
                </div>
                <div class="moreNfo__item">
                    <div class="moreNfo__title">Release Date:</div>
                    <span href="#" class="link moreNfo__content">${release_date}</span>
                </div>
                <div class="moreNfo__item">
                    <div class="moreNfo__title">Lenght Movie Time:</div>
                    <span href="#" class="link moreNfo__content">${runtime} munites</span>
                </div>
            </div>
        </div>
    `
};

function createCasts(casts, type) {
    let castArray;
    if(type === 'limit') {
        castArray = casts.length > 10 ? casts.slice(0, 10) : [...casts];
    }
    else {
        castArray = [...casts];
    }
    
    return `

        <div class="castList">
            ${castArray.map(cast => `
                <div class="cast">
                    <div class="cast__avatar mr-3">
                        <img src="${imgURL + cast.profile_path}" class="img-fluid" alt="avatar">
                    </div>
                    <a onclick="selectedDetailCast('${cast.credit_id}')" href="cast.html" class="link cast__name">${cast.name}</a>
                    <div class="cast__character">Character: <span>${cast.character}</span></div>
                </div>
            `).join('')}
        </div>
    `
};

function createImageAndVideo(imgs) {
    const imgArray = imgs.length > 10 ? imgs.slice(0, 9) : [...imgs];

    return `
        <div class="library">
        ${imgArray.map(img => `
                <img src="${imgURL + img.file_path}" alt="item1" class="library__item">
            `).join('')}
            
        </div>
    `
};

function createOverviewTabContent(overview, imgArray, castsArray) {
    return `
        <div class="tab-pane fade show active" id="nav-overview">
            ${overview}
            <div class="heading mt-3">
                <div class="heading__title">video & photos</div>
                <a class="link heading__link" href="javascript:void(0)">all video & photos ></a>
            </div>
            ${createImageAndVideo(imgArray)}

            <div class="heading mt-3">
                <div class="heading__title">cast</div>
                <a class="link heading__link cast-link" href="javascript:void(0)">full cast ></a>
            </div>
            ${createCasts(castsArray, 'limit')}
        </div>
    `
}

function createReviewsTabContent(reviews) {
    return `
    <div class="tab-pane fade mb-4" id="nav-reviews" >
        <div class="heading-reviews mb-4">
            <p class="heading-reviews__title">Related Movies To</p>
            <h4 class="heading-reviews__movieName">Related Movies To</h4>
        </div>
        <div class="topBar">
            <p class="topBar__info">Found <span class="topBar__highlight"> movies</span> in total</p>
        </div>

        <div class="reviews">
            ${reviews.map(review => `
                <div class="review pb-3 pt-3">
                    <div class="review__reviewer mb-4">
                        <div class="review__avatar"><img src="../jquery-movies-website/assets/images/default_avatar.png" alt="avatar"/></div>
                        <div class="review__title">A review by <a href="javascript:void(0)" class="link">${review.author}</a></div>
                    </div>
                    <div class="review__content">
                        <p>${review.content.length > 500 ? review.content.slice(0, 500) + ' ...' : review.content}</p>
                        <p style='color: #ffffff'>Link detail: <a href="${review.url}" class="link">${review.url}</a></p>
                    </div>
                </div>
            `).join('')}
            
        </div>
    </div>
    `
}
function createCastsTabContent(casts) {
    return `
    <div class="tab-pane fade mb-4" id="nav-casts" >
        <div class="heading-reviews mb-4">
            <p class="heading-reviews__title">Cast of</p>
            <h4 class="heading-reviews__movieName ml-4">This Movie</h4>
        </div>
        

        <div class="casts">
            <div class="heading mt-3">
                <div class="heading__title">cast</div>
            </div>

            ${createCasts(casts, 'full')}
            
        </div>
    </div>
    `
}

function createNfoTop(images, title, release_date, vote_average, vote_count) {

    // 2: có nửa sao, 1: ko
    const starArray = isFloat(vote_average) ? new Array(Math.floor(vote_average)).fill(2) : new Array(vote_average).fill(1);
    const starEmptyArray = new Array( starArray[0] === 1 ? (10 - starArray.length) : (9 - starArray.length) ).fill(0);
    
    return `
        <div class="col-sm-4">
            <div class="card" >
                <img src="${imgURL + images.posters[0].file_path}" class="card-img-top" alt="poster">
                <div class="card-body car-body--border">
                    <button type="button" class="btn btn--text btn--red btn-block mb-3"><i class="fas fa-film fas--white mr-2"></i> watch trailer</button>
                    <button type="button" class="btn btn--text btn--yellow btn-block"><i class="mr-2 fas fa-business-time fas--black"></i> buy ticket</button>
                </div>
            </div>
        </div>

        <div class="col-sm-8 pl-5">
            <div class="info-top">
                <h2 class="info-top__title mb-4">${title} <span>${release_date.slice(0, release_date.indexOf('-'))}</span></h2>
                <div class="btnIcon mr-5">
                    <i class="fas fa-heart fas--red fas--button mr-2"></i>
                    <span class = "btnIcon__text">add to favorite</span>
                </div>
                <div class="btnIcon">
                    <i class="fas fa-share-alt fas--red fas--button mr-2"></i>
                    <span class = "btnIcon__text">share</span>
                </div>

                <div class="topBar mt-5">
                    <div class="topBar__review">
                            <i class="fas fa-star fas--yellow fas--large"></i>
                            <div class="topBar__reviewCount">
                                <p class="topBar__text score"><span>${vote_average}</span> /10</p>
                                <p class="topBar__text topBar__text--blue vote">${vote_count} Votes</p>
                            </div>
                    </div>
                    <p class="topBar__info topBar__info--border">
                        Rate this movie: 
                        ${
                            (starArray[0] === 1) ?
                                starArray.map(star => `<i class="ml-2 fas fa-star fas--yellow fas--large"></i>`).join('')
                                + starEmptyArray.map(starEmpty => `<i class="ml-2 far fa-star fas--large"></i>`).join('')
                            :
                                starArray.map(star => `<i class="ml-2 fas fa-star fas--yellow fas--large"></i>`).join('')
                                + '<i class="fas ml-2 fa-star-half-alt fas--large fas--yellow"></i>'
                                + starEmptyArray.map(starEmpty => `<i class="ml-2 far fa-star fas--large"></i>`).join('')
                        }
                        
                        
                    </p>
                </div>

                <nav class="navTab">
                    <div class="nav" id="nav-tab">
                        <a class="nav-item nav-link active" id="nav-overview-tab" href="#nav-home" >Overview</a>
                        <a class="nav-item nav-link" id="nav-reviews-tab" href="javascript:void(0)">Reviews</a>
                        <a class="nav-item nav-link" id="nav-casts-tab" href="javascript:void(0)">Casts</a>
                        
                    </div>
                </nav>

                
            </div>
        </div>

    `
}

function createOutputMovieDetail(movieResponse, casts, images, reviewsResponse) {

    const { title, release_date, vote_average, vote_count, overview } = movieResponse;
    const directors = casts.crew.filter(item => item.department === 'Directing');
    const castsArray = casts.cast;
    const imgArray = images.backdrops;
    const { results: reviews } = reviewsResponse;

    let topDetail = `
        ${createNfoTop(images, title, release_date, vote_average, vote_count)}
    `;
    $('.movieNfo .row').empty();
    $('.movieNfo .row').append(topDetail);

    let movieDetailOutput = `
        <div class="col-sm-4"></div>
        <div class="col-sm-6 pl-5">
                <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
                        ${createOverviewTabContent(overview, imgArray, castsArray)}

                        ${createReviewsTabContent(reviews)}
                        
                        ${createCastsTabContent(castsArray)}
                </div>
        </div>
        
        
    `+ createMoreNfo(movieResponse, directors);


    $('#main-movie .row').empty();
    $('#main-movie .row').append(movieDetailOutput);

    $('#nav-reviews-tab').click(function(){
        $('#nav-tab .nav-link').removeClass('active');
        $(this).addClass('active');

        $('#nav-tabContent .tab-pane').removeClass('show active');
        $('#nav-reviews').addClass('show active');
    });

    $('#nav-overview-tab').click(function(){
        $('#nav-tab .nav-link').removeClass('active');
        $(this).addClass('active');

        $('#nav-tabContent .tab-pane').removeClass('show active');
        $('#nav-overview').addClass('show active');
    });

    $('#nav-casts-tab').click(function(){
        $('#nav-tab .nav-link').removeClass('active');
        $(this).addClass('active');

        $('#nav-tabContent .tab-pane').removeClass('show active');
        $('#nav-casts').addClass('show active');
    });

    $('#nav-overview .cast-link').click(function () {
        $('#nav-tab .nav-link').removeClass('active');
        $('#nav-casts-tab').addClass('active');

        $('#nav-tabContent .tab-pane').removeClass('show active');
        $('#nav-casts').addClass('show active');
    })
}

async function getMovie() {
    const idMovie = sessionStorage.getItem('idMovieDetail');

    let response = await fetch(`https://api.themoviedb.org/3/movie/${idMovie}?api_key=${key}&language=en-US`);
    let moviesWrapper = await response.json();

    return moviesWrapper;
}

async function getReviews() {
    const idMovie = sessionStorage.getItem('idMovieDetail');

    let response = await fetch(`https://api.themoviedb.org/3/movie/${idMovie}/reviews?api_key=${key}&language=en-US`);
    let reviewsWrapper = await response.json();

    return reviewsWrapper;
}

async function getCasts() {
    const idMovie = sessionStorage.getItem('idMovieDetail');

    let response = await fetch(`https://api.themoviedb.org/3/movie/${idMovie}/credits?api_key=${key}`);
    let casts = await response.json();

    return casts;
}

async function getImages() {
    const idMovie = sessionStorage.getItem('idMovieDetail');

    let response = await fetch(`https://api.themoviedb.org/3/movie/${idMovie}/images?api_key=${key}`);
    let images = await response.json();

    return images;
}


function selectedDetailCast(idString) {
    sessionStorage.setItem('idCastDetail', idString);
}

// ----------- check số float or interger ----------
function isInt(n){
    return Number(n) === n && n % 1 === 0;
}

function isFloat(n){
    return Number(n) === n && n % 1 !== 0;
}
